######################################################
# THIS PROGRAM WILL BE IN THE RECEIVING END -- THE PIT
######################################################

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ("192.168.1.50", 5002)
# server_address = ("127.0.0.1", 5002)

print("Starting up on: "+ server_address[0] + " (port "+ str(server_address[1]) +").")

sock.bind(server_address)
sock.listen(1)

while True:
	# Find connections
	connection, client_address = sock.accept()
	try:
		data = connection.recv(999)
		print(data)


	except:
		connection.close()