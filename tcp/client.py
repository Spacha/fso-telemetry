#################################
# THIS PROGRAM WILL BE IN THE CAR
#################################

import socket
import sys
import time

import random

# readData --> read data from ECU and save to buffer
# sendData --> send the data from the buffer to the server

SERVER_HOST = "192.168.1.50"
SERVER_PORT = 5002

LOCAL_SERVER_HOST = "127.0.0.1"
LOCAL_SERVER_PORT = 5002

# SERVER_ADDR = (LOCAL_SERVER_HOST, LOCAL_SERVER_PORT)
SERVER_ADDR = (SERVER_HOST, SERVER_PORT)

dataBuffer = []
sock = None


def readData():
	''' Get data from ECU '''

	# generate some dummy data
	dataBuffer.append(random.randint(0,100))

def storeData():
	# ...
	return

def sendData():
	''' Send all data from buffer to the server '''
	global sock

	# join the data into a single (csv) string
	# dataString = ', '.join()
	dataString = "DATA: "
	for k, e in enumerate(dataBuffer):
		dataString += ' ' + str(e)

	try:
		server_connect()

		#message = raw_input('Message: ')
		#if message=='quit':
		#	break

		sock.sendall(dataString)

	except:

		e = sys.exc_info()[1]
		print("Failed to send data (error: "+ str(e) +").")
		return False

	print("Data sent successfully.")
	return True


def clearBuffer():
	''' Clear the data buffer '''
	# TODO: dont clear if sending and/or storing failed
	del dataBuffer[:]
	print("Buffer cleared.")

	return


#############
# 	SERVER
#############

def server_init():
	''' Create a TCP/IP socket '''
	global sock

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print("Connecting to: "+ SERVER_ADDR[0] + " (port "+ str(SERVER_ADDR[1]) +").")


def server_connect():
	''' Connect to the server '''
	global sock
	
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect(SERVER_ADDR)

	except:
		print("Failed to connect to the server...")


########################################
# 	The Program begins
########################################

# Init the connection to the server
server_init()


#################
# 	MAIN LOOP
#################

while True:
	readData()

	# Try to send the data. If successful, clear the buffer.
	if sendData():
		clearBuffer()

	time.sleep(1)

sock.close()
